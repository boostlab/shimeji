﻿using UnityEngine;

public class Target : MonoBehaviour
{
	Transform myTransform;

	void Awake()
	{
		UniqueGameObject.Target = this;
		myTransform = GetComponent<Transform>();
	}

	public void UpdatePosition( Vector2 position )
	{
		myTransform.position = position;
	}

	public Vector2 GetPosition()
	{
		return myTransform.position;
	}
}
