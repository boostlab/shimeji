﻿using UnityEngine;

public class Muzzle : MonoBehaviour
{
	Transform myTransform;

	void Awake()
	{
		myTransform = GetComponent<Transform>();
	}

	public Vector2 GetPosition()
	{
		return myTransform.position;
	}

	public float GetShootAngle()
	{
		var targetPosition = UniqueGameObject.Target.GetPosition();
		var direction = ( targetPosition - (Vector2) myTransform.position ).normalized;
		return Mathf.Atan2( direction.y, direction.x ) * Mathf.Rad2Deg;
	}
}
