﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TargetDragArea : MonoBehaviour, IPointerDownHandler, IDragHandler
{
	void IPointerDownHandler.OnPointerDown( PointerEventData eventData )
	{
		UpdateTargetPosition( eventData );
	}

	void IDragHandler.OnDrag( PointerEventData eventData )
	{
		UpdateTargetPosition( eventData );
	}

	void UpdateTargetPosition( PointerEventData eventData )
	{
		UniqueGameObject.Target.UpdatePosition( UniqueGameObject.StableAspect.ScreenToTarget( eventData.position ) );
	}
}
