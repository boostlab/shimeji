﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
	[SerializeField]
	float speed = 1;

	[SerializeField]
	SpriteRenderer myRenderer = null;

	Transform myTransform;
	Rigidbody2D myRigidbody;

	void Awake()
	{
		myTransform = GetComponent<Transform>();
		myRigidbody = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		UpdateBeing();
		UpdateVelocity();
	}

	public void Initialize( Vector2 position, float angle )
	{
		myTransform.position = position;
		myTransform.localEulerAngles = new Vector3( 0, 0, angle );
	}

	void UpdateVelocity()
	{
		myRigidbody.velocity = myTransform.right * speed;
	}

	void UpdateBeing()
	{
		if( myRenderer.isVisible ) return;
		Destroy( gameObject );
	}
}
