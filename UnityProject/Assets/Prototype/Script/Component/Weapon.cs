﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
	[SerializeField]
	float shootInterval = 0.1f;
	[SerializeField]
	Bullet bulletPrefab = null;

	[SerializeField]
	Sprite rightSprite = null;
	[SerializeField]
	Sprite leftSprite = null;

	[SerializeField]
	SpriteRenderer myRenderer = null;
	[SerializeField]
	Muzzle myMuzzle = null;

	Transform myTransform;

	float shootTime;

	void Awake()
	{
		myTransform = GetComponent<Transform>();
	}

	void Update()
	{
		UpdateRotation();
		UpdateShoot();
	}

	void UpdateRotation()
	{
		var targetPosition = UniqueGameObject.Target.GetPosition();
		var direction = ( targetPosition - (Vector2) myTransform.position ).normalized;
		if( direction.x >= 0 )
		{
			myRenderer.sprite = rightSprite;
		}
		else
		{
			myRenderer.sprite = leftSprite;
		}
		myTransform.localEulerAngles = new Vector3( 0, 0, Mathf.Atan2( direction.y, direction.x ) * Mathf.Rad2Deg );
	}

	void UpdateShoot()
	{
		shootTime -= Time.deltaTime;
		if( shootTime > 0 ) return;
		shootTime += shootInterval;

		Instantiate( bulletPrefab ).Initialize( myMuzzle.GetPosition(), myMuzzle.GetShootAngle() );
	}
}
