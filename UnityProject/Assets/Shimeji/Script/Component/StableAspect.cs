﻿using UnityEngine;

public class StableAspect : MonoBehaviour
{
	[SerializeField]
	Vector2 targetSize = new Vector2();
	[SerializeField]
	float pixelPerUnit = 0;

	Camera myCamera;

	Vector2 screenSize;
	Vector2 screenScale;
	Vector2 screenOffset;
	Vector2 screenToTargetScale;

	void Awake()
	{
		UniqueGameObject.StableAspect = this;
		myCamera = GetComponent<Camera>();
	}

	void Start()
	{
		UpdateAspect();
	}

	void Update()
	{
		//if( !Application.isEditor ) return;
		UpdateAspect();
	}

	public Vector2 ScreenToTarget( Vector2 screenPositon )
	{
		screenPositon.Scale( screenToTargetScale );
		return screenOffset + screenPositon;
	}

	void UpdateAspect()
	{
		UpdateScreenSize();
		UpdateScreenScale();
		UpdateScreenToTargetScale();
		UpdateCameraSize();
		UpdateCameraScale();
	}

	void UpdateScreenSize()
	{
		screenSize = new Vector2( Screen.width, Screen.height );
	}

	void UpdateScreenScale()
	{
		var scaleW = targetSize.x / Screen.width;
		var scaleH = targetSize.y / Screen.height;
		if( scaleW > scaleH )
		{
			screenScale = new Vector2( 1, targetSize.y / ( Screen.height * scaleW ) );
		}
		else
		{
			screenScale = new Vector2( targetSize.x / ( Screen.width * scaleH ), 1 );
		}
	}

	void UpdateScreenToTargetScale()
	{
		var virtualScreenSize = new Vector2( targetSize.x / screenScale.x, targetSize.y / screenScale.y );
		screenOffset = -new Vector2( ( virtualScreenSize.x - targetSize.x ) / 2, ( virtualScreenSize.y - targetSize.y ) / 2 );
		screenToTargetScale = new Vector2( virtualScreenSize.x / screenSize.x, virtualScreenSize.y / screenSize.y );
	}

	void UpdateCameraSize()
	{
		myCamera.orthographicSize = targetSize.y / 2f / pixelPerUnit;
	}

	void UpdateCameraScale()
	{
		myCamera.rect = new Rect( ( 1f - screenScale.x ) / 2f, ( 1f - screenScale.y ) / 2f, screenScale.x, screenScale.y );
	}
}
